#!/bin/bash

echo "Criando as imagens . . . ."
sleep 10

docker build -t edivancarvalho/projeto-backend:1.0 backend/.
docker build -t edivancarvalho/projeto-database:1.0 database/.

echo "Realizando opush das imagens .. .. . . "
sleep 10

docker push edivancarvalho/projeto-backend:1.0
docker push edivancarvalho/projeto-database:1.0

echo "CRiando serviços no cluster kubernetes . . . . "
sleep 10

kubectl apply -f ./services.yml

echo "Criando os deployment . . . "
sleep 10

kubectl apply -f ./deployment.yml

